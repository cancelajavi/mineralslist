var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FirebaseService } from './services/firebase.service';
var AppComponent = (function () {
    function AppComponent(_firebaseService) {
        this._firebaseService = _firebaseService;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._firebaseService.getMinerals().subscribe(function (minerals) {
            _this.minerals = minerals;
            console.log(minerals);
        });
        this._firebaseService.getLocations().subscribe(function (locations) {
            _this.locations = locations;
        });
    };
    AppComponent.prototype.changeState = function (state, key) {
        if (key) {
            this.activeKey = key;
        }
        this.appState = state;
    };
    AppComponent.prototype.filterLocation = function (location) {
        var _this = this;
        this._firebaseService.getMinerals(location).subscribe(function (minerals) {
            _this.minerals = minerals;
        });
    };
    AppComponent.prototype.addMineral = function (description, imgurl, location, name, gotIt) {
        var newMineral = {
            description: description,
            imgurl: imgurl,
            location: location,
            name: name,
            gotIt: gotIt
        };
        this._firebaseService.addMineral(newMineral);
        this.appState = 'default;';
    };
    AppComponent.prototype.showEdit = function (mineral) {
        this.changeState('edit', mineral.$key);
        this.activeDescription = mineral.description;
        this.activeImgurl = mineral.imgurl;
        this.activeLocation = mineral.location;
        this.activeName = mineral.name;
        this.activeGotIt = mineral.gotIt;
    };
    AppComponent.prototype.updateMineral = function () {
        var updMineral = {
            description: this.activeDescription,
            imgurl: this.activeImgurl,
            location: this.activeLocation,
            name: this.activeName,
            gotIt: this.activeGotIt
        };
        this._firebaseService.updateMineral(this.activeKey, updMineral);
    };
    AppComponent.prototype.deleteMineral = function (key) {
        this._firebaseService.deleteMineral(key);
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css'],
        providers: [FirebaseService]
    }),
    __metadata("design:paramtypes", [FirebaseService])
], AppComponent);
export { AppComponent };
//# sourceMappingURL=../../../src/app/app.component.js.map