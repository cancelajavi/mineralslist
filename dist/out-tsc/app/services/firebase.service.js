var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
import 'rxjs/add/operator/map';
var FirebaseService = (function () {
    function FirebaseService(_af) {
        this._af = _af;
    }
    FirebaseService.prototype.getMinerals = function (location) {
        if (location === void 0) { location = null; }
        if (location != null && location != "") {
            this.minerals = this._af.database.list('/minerals', {
                query: {
                    orderByChild: 'location',
                    equalTo: location
                }
            });
            return this.minerals;
        }
        else {
            this.minerals = this._af.database.list('/minerals');
            return this.minerals;
        }
    };
    FirebaseService.prototype.getLocations = function () {
        this.locations = this._af.database.list('/locations');
        return this.locations;
    };
    FirebaseService.prototype.addMineral = function (newMineral) {
        return this.minerals.push(newMineral);
    };
    FirebaseService.prototype.updateMineral = function (key, updMineral) {
        return this.minerals.update(key, updMineral);
    };
    FirebaseService.prototype.deleteMineral = function (key) {
        return this.minerals.remove(key);
    };
    return FirebaseService;
}());
FirebaseService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [AngularFire])
], FirebaseService);
export { FirebaseService };
//# sourceMappingURL=../../../../src/app/services/firebase.service.js.map