import { MineralsListPage } from './app.po';

describe('minerals-list App', function() {
  let page: MineralsListPage;

  beforeEach(() => {
    page = new MineralsListPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
