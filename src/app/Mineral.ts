export interface Mineral{
    $key?: string;
    description?:string;
    imgurl?:string;
    location?:string;
    name?:string;
    gotIt?:boolean;
}
