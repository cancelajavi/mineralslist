import { Component,OnInit } from '@angular/core';
import {FirebaseService} from './services/firebase.service';
import {Mineral} from './Mineral';
import {Location} from './Location';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [FirebaseService]
})
export class AppComponent implements OnInit{
  minerals:Mineral[];
  locations:Location[];
  appState: string;
  activeKey: string;

  activeDescription:string;
  activeImgurl:string;
  activeLocation:string;
  activeName:string;
  activeGotIt:boolean;

  constructor(private _firebaseService:FirebaseService) {

  }

  ngOnInit(){
    this._firebaseService.getMinerals().subscribe(minerals => {
      this.minerals = minerals;
      console.log(minerals);
    });

    this._firebaseService.getLocations().subscribe(locations => {
      this.locations = locations;
    });
  }

  changeState(state, key){
    if(key){
      this.activeKey = key;
    }
    this.appState = state;
  }

  filterLocation(location){
    this._firebaseService.getMinerals(location).subscribe(minerals => {
      this.minerals = minerals;
    });
  }

  addMineral(
    description:string,
    imgurl:string,
    location:string,
    name:string,
    gotIt:string
  ){
    var newMineral = {
      description:description,
      imgurl:imgurl,
      location:location,
      name:name,
      gotIt:gotIt
    };
    this._firebaseService.addMineral(newMineral);
    this.appState = 'default;'
  }

  showEdit(mineral){
    this.changeState('edit', mineral.$key);
    this.activeDescription = mineral.description;
    this.activeImgurl = mineral.imgurl;
    this.activeLocation = mineral.location;
    this.activeName = mineral.name;
    this.activeGotIt = mineral.gotIt;
  }

  updateMineral(){
    var updMineral = {
      description:this.activeDescription,
      imgurl:this.activeImgurl,
      location:this.activeLocation,
      name:this.activeName,
      gotIt: this.activeGotIt
    };

    this._firebaseService.updateMineral(this.activeKey, updMineral);
  }

  deleteMineral(key){
    this._firebaseService.deleteMineral(key);
  }


}
