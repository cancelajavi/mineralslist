import {Injectable} from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import 'rxjs/add/operator/map';
import {Mineral} from '../Mineral';
import {Location} from '../Location';

@Injectable()
export class FirebaseService{
    minerals: FirebaseListObservable<Mineral[]>;
    locations: FirebaseListObservable<Location[]>;

    constructor(private _af: AngularFire){

    }

    getMinerals(location:string = null){
        if(location != null && location != ""){
          this.minerals = this._af.database.list('/minerals', {
            query: {
              orderByChild: 'location',
              equalTo: location
            }
          }) as
          FirebaseListObservable<Mineral[]>
          return this.minerals;
        } else{
        this.minerals = this._af.database.list('/minerals') as
        FirebaseListObservable<Mineral[]>
        return this.minerals;
      }
    }

    getLocations(){
        this.locations = this._af.database.list('/locations') as
        FirebaseListObservable<Location[]>
        return this.locations;
    }

    addMineral(newMineral){
      return this.minerals.push(newMineral);
    }
    updateMineral(key, updMineral){
      return this.minerals.update(key, updMineral);
    }
    deleteMineral(key){
      return this.minerals.remove(key);
    }
}
